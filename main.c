#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <time.h>

#include "kiaic.h"

/* Assumptions:
	This code uses the strategy of maximizing knowledge by checking every possible n step move and evaluating the maximum number of unique scanned squares.
	
	Direction Convention used in this code:
	+----+------+----+
	| NW | N    | NE |
	+----+------+----+
	| W  | Stay | E  |
	+----+------+----+
	| SW | S    | SE |
	+----+------+----+
	
	x--> horizontal axis
	y--> vertical axis
	+---+---+---+
	| 0 | 1 | 2 |
	+---+---+---+
	| 1 |   |   |
	+---+---+---+
	| 2 |   |   |
	+---+---+---+
*/

enum Directions {
	NorthWest,
	North,
	NorthEast,
	West,
	Stay,
	East,
	SouthWest,
	South,
	SouthEast	
};

struct moveTree{
	struct moveTree *parent;
	struct moveTree *child[9];
	int currChild;
	int moveDir;
	int moveNth;
	int xCurr;
	int yCurr;
	int totalInfo;
};

//Function Prototypes
int CreateRoute(int moveCount, int boardWidth, int boardHeight, struct moveTree *root);
struct moveTree * BuildNode(struct moveTree *parentNode, int moveMade, int moveCount, int x, int y);
int FindMaxNum(struct moveTree *root, int boardWidth, int boardHeight);
int CalcUniSqr(struct moveTree *root, int boardWidth, int boardHeight, int *board);
void SeekMax(struct moveTree *node, int *moveList, int maxNum, int maxMove);
void MakeList(struct moveTree *node, int *moveList, int maxMove);
int SaveTheTrees(struct moveTree *root);
int DecideFireDir(int boardWidth, int boardHeight, int x, int y);
int CheckTableLimit(int x, int y, int delta, int boardWidth, int boardHeight);

int xDirToVal(int k);
int yDirToVal(int k);
int DirToKIAI(int x);

void test(struct moveTree *node);
void printBoard(int *board, int boardWidth, int boardHeight);

//Main Start
int main(int argc, char *argv[])
{
	int i = 0, j = 0, k, maxNum = 0, maxMove, boardWidth, boardHeight, bestDir, totalTurn = 0;
	int *moveList;
	struct moveTree root;
	int totalNodes = 0;
	
	// Initialize random number generator
	srand(time(NULL));
		
	root.currChild = 0;
	root.parent = NULL;
	root.xCurr = 0;
	root.yCurr = 0;
	
	KIAI_GameConfig c = KIAI_Connect("127.0.0.1", "SaitoBot", 1, 0);
	
	boardWidth = c.Width;
	boardHeight = c.Height;
	maxMove = c.MaxMoves;
	
	printf("Connected..\n");
	
	while(true) {
		// Bot waits for its turn
		printf("Waiting for the turn...\n");
		KIAI_TurnStatus t = KIAI_WaitForTurn();
		root.currChild = 0;
		
		printf("Saito's Turn... Starting to the turn... (%d)\n", t.MovesLeft);
		
		printf("Current Position: (%d, %d) - Moves Left: %d\n", t.X, t.Y, t.MovesLeft);
			
		root.xCurr = t.X;
		root.yCurr = t.Y;
		
		printf("Calculating the best move chain...\n");
		
		totalNodes = CreateRoute(maxMove, boardWidth, boardHeight, &root);				
		printf("Total Calculated Nodes: %d\n", totalNodes);
		
		maxNum = FindMaxNum(&root, boardWidth, boardHeight);
		printf("Maximum number of total unique square info: %d\n", maxNum);
		
		moveList = (int*) malloc(sizeof(int) * maxMove);
		assert(moveList != NULL);
		printf("Memory allocation for 'moveList' successful...\n");
		
		SeekMax(&root, moveList, maxNum, maxMove);
		printf("Calculation done...\n");
		
		printf("Best move directions to be followed:\n");
		printf("(Saito): %d - %d - %d -%d - %d\n", moveList[0], moveList[1], moveList[2], moveList[3], moveList[4]);				
		printf("(Kiai): %d - %d - %d -%d - %d\n", DirToKIAI(moveList[0]), DirToKIAI(moveList[1]), DirToKIAI(moveList[2]), DirToKIAI(moveList[3]), DirToKIAI(moveList[4]));
		
		totalNodes = SaveTheTrees(&root);
		printf("Saved the trees %d...\n", totalNodes);
			
		while(t.MovesLeft > 0){
						
			printf("Scanning radar for enemies...\n");
			// Fire if there is anybody around
			for(i = 0; i < 8; i++) {
				if(t.Radar[i] > 1) {
					printf("Enemy spotted at: t.Radar[%d]: %d\n", i, t.Radar[i]);
					KIAI_Fire(&t, i);
					break;
				}
			}
			
			printf("Starting to move...\n");
			while(true) {
				printf("Will try to move to the direction:\n");
				printf("(Saito): %d \n", moveList[maxMove - t.MovesLeft]);
				printf("(Kiai): %d \n", DirToKIAI(moveList[maxMove - t.MovesLeft]));
				
				int direction = DirToKIAI(moveList[maxMove - t.MovesLeft]);
				if(t.MovesLeft == 1) direction = (rand() % 8);
				if (t.Radar[direction] == 0) {
					printf("Moving right now to -No obstacle-: %d\n", direction);
					KIAI_Move(&t, direction);
					break;
				}
			}
			
			// Or fire randomly if nobody's around
			if(t.MovesLeft == 0&& t.FiresLeft > 0) {
				printf("Last move... Firing for the longest route...\n");
				KIAI_Fire(&t, DirToKIAI(DecideFireDir(boardWidth, boardHeight, t.X, t.Y)));
				break;
			}
		}
		
		// End turn
		maxNum = 0;
		//TODO: if(moveList == NULL)
		printf("De-allocating the memory for the best move list...\n");
		free(moveList);
		printf("Ending turn...\n");
		KIAI_EndTurn();
	}
	printf("Disconnecting...\n");
	KIAI_Disconnect();
	return 0;
}

int CreateRoute(int moveCount, int boardWidth, int boardHeight, struct moveTree *root)
{
	/* Recursively builds a tree which contains every possible move within the board and maxMove limits */
	
	int i, numNodes = 1;
	struct moveTree *newChild;
	
	if(moveCount > 0){
		for(i = NorthWest; i <= SouthEast; i++){
			if(CheckTableLimit(root->xCurr, root->yCurr, i, boardWidth, boardHeight)){

				newChild = BuildNode(root, i, moveCount, root->xCurr + xDirToVal(i), root->yCurr + yDirToVal(i));
				assert(newChild != NULL);

				numNodes += CreateRoute(moveCount - 1, boardWidth, boardHeight, newChild);
			}
		}
	}
	return numNodes;
}

int CheckTableLimit(int x, int y, int delta, int boardWidth, int boardHeight)
{
	int x_new, y_new;
	
	x_new = x + xDirToVal(delta);
	y_new = y + yDirToVal(delta);
	
	if(x_new < boardWidth && x_new >= 0 && y_new < boardHeight && y_new >= 0){
		return 1;
	} else{
		return 0;
	}
}

struct moveTree * BuildNode(struct moveTree *parentNode, int moveMade, int moveCount, int x, int y)
{
	struct moveTree *newChild;
	
	newChild = (struct moveTree *) malloc(sizeof(struct moveTree));
	assert(newChild != NULL);

	assert(parentNode->currChild < 9);
	parentNode->child[parentNode->currChild] = newChild;
	parentNode->currChild++;

	newChild->parent = parentNode;
	newChild->moveDir = moveMade;
	newChild->moveNth = moveCount;
	newChild->currChild = 0;
	newChild->xCurr = x;
	newChild->yCurr = y;

	return newChild;
}

int FindMaxNum(struct moveTree *root, int boardWidth, int boardHeight)
{
	int i, j, currNum = 0, maxNum = 0;
	int *board;
	
	if(root->currChild != 0){
		for(i = 0; i < root->currChild; i++){
			currNum = FindMaxNum(root->child[i], boardWidth, boardHeight);
			if(currNum > maxNum) maxNum = currNum;
		}
	} else if(root->currChild == 0){
		board = (int *) malloc(sizeof(int) * boardWidth * boardHeight);
		assert(board != NULL);
		for(j = 0; j < boardWidth * boardHeight; j++){
			board[j] = 0;
		}
		currNum = CalcUniSqr(root, boardWidth, boardHeight, board);
		if(currNum > maxNum) maxNum = currNum;
		free(board);
	}
	
	return maxNum;
}

int CalcUniSqr(struct moveTree *node, int boardWidth, int boardHeight, int *board)
{
	int i, x, y, numKnow = 0;
	
	if(node->parent != NULL){
		board[node->xCurr * boardWidth + node->yCurr] = 1;
		CalcUniSqr(node->parent, boardWidth, boardHeight, board);
	} else if(node->parent == NULL){
		board[node->xCurr * boardWidth + node->yCurr] = 1;
		return 0;
	}

	if(node->currChild == 0){
		for(x = 0; x < boardWidth; x++){
			for(y = 0; y < boardHeight; y++){
				if(board[x * boardWidth + y] == 1){
					for(i = NorthWest; i <= SouthEast; i++){
						if(CheckTableLimit(x, y, i, boardWidth, boardHeight)){
							if(board[(x + xDirToVal(i)) * boardWidth + (y + yDirToVal(i))] == 0){
								board[(x + xDirToVal(i)) * boardWidth + (y + yDirToVal(i))] = 2;
							}
						}
					}
				}
			}
		}
		for(i = 0; i < boardWidth * boardHeight; i++){
			if(board[i] == 1 || board[i] == 2) numKnow++;
		}
		//if(numKnow == 34) printBoard(board, boardWidth, boardHeight);
		//printf("%d\n", node->moveNth);
		node->totalInfo = numKnow;
	}
	return numKnow;
}

void SeekMax(struct moveTree *node, int *moveList, int maxNum, int maxMove)
{
	int i;
	
	if(node->currChild != 0 && node->totalInfo != maxNum){
		for(i = 0; i < node->currChild; i++){
			SeekMax(node->child[i], moveList, maxNum, maxMove);
		}
	} else if(node->currChild == 0 && node->totalInfo == maxNum){
		MakeList(node, moveList, maxMove);
	}
}

void MakeList(struct moveTree *node, int *moveList, int maxMove)
{
	if(node->parent != NULL){
		moveList[(maxMove) - node->moveNth] = node->moveDir;
		MakeList(node->parent, moveList, maxMove);
	}
}

int DecideFireDir(int boardWidth, int boardHeight, int x, int y)
{
	int i, j, k, numSqr, maxSqr = 0, maxIndex = 0;
	int xNew, yNew;
	int board[boardWidth][boardHeight];
	int list[9] = {0,0,0,0,0,0,0,0,0};
	
	for(i = 0; i < boardWidth; i++){
		for(j = 0; j < boardHeight; j++){
			board[i][j] = 0;			
		}
	}
	
	for(i = NorthWest; i <= SouthEast && i != Stay; i++){
		xNew = x;
		yNew = y;
		numSqr = 0;		
		
		while(CheckTableLimit(xNew, yNew, i, boardWidth, boardHeight)){

			board[xNew + xDirToVal(i)][yNew + yDirToVal(i)] = 1;
			xNew += xDirToVal(i);
			yNew += yDirToVal(i);
		}
		
		for(k = 0; k < boardWidth; k++){
			for(j = 0; j < boardHeight; j++){
				if(board[k][j] == 1) numSqr++;
			}
		}

		list[i] = numSqr;
		for(k = 0; k < boardWidth; k++){
			for(j = 0; j < boardHeight; j++){
				board[k][j] = 0;
			}
		}
	}
	
	for(i = 0; i < 9; i++){
		if(list[i] > maxSqr) maxSqr = list[i];
	}
	
	for(i = 0; i < 9; i++){
		if(list[i] == maxSqr) maxIndex = i;
	}
	
	return maxIndex;
}

int SaveTheTrees(struct moveTree *root)
{
	int j = 1;
	
	if(root->currChild !=0){
		int i;
		
		for(i = 0; i < root->currChild; i++){
			j += SaveTheTrees(root->child[i]);
		}
	}

	if(root->parent != NULL) free(root);

	return j;
}

int xDirToVal(int k)
{
	switch(k % 3){
		case 0: return (-1);
		case 1: return (0);
		case 2: return (1);
		default: return 0;
	}
}

int yDirToVal(int k)
{
	switch(k){
		case 0:
		case 1:
		case 2: return (-1);
		case 3:
		case 4:
		case 5: return (0);
		case 6:
		case 7:
		case 8: return (1);
		default: return 0;
	}
}

int DirToKIAI(int x)
{
	switch(x){
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 7;
		case 4: return 8;
		case 5: return 3;
		case 6: return 6;
		case 7: return 5;
		case 8: return 4;
		default: return 0;
	}
}

void printBoard(int *board, int boardWidth, int boardHeight){
	int x, y;
	printf("\n");
	for(y = 0; y < boardHeight; y++){
		for(x = 0; x < boardWidth; x++){
			printf("%d ", board[x * boardWidth + y]);
		}
		printf("\n");
	}
}

void test(struct moveTree *node)
{
	if(node->parent != NULL){
		printf("\n%d\n", node->moveDir);
		test(node->parent);
	}
}


